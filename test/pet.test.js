const Pet = require('../src/pet');

let virtualPet;

beforeEach(() => {
    virtualPet = new Pet();
  });


describe('constructor', () => {
    it('returns an object', () => {
      expect(new Pet('Albie')).toBeInstanceOf(Object);
    });
    it('sets the name property', () => {
        expect(new Pet().name).toBe('Albie');
      });
    it('has a initial age of 0', () => {
        expect(new Pet().age).toBe(0);
    });
    it('should set a default hunger of 0', () => {
      expect(new Pet().hunger).toBe(0);
  })
    it('should set a default fitness of 10', () => {
      expect(new Pet().fitness).toBe(10);
  })
});

describe('growUp', () => {
  it('should make the pets age increase by 1', () => {
      virtualPet.growUp();
      expect(virtualPet.age).toBe(1);
      virtualPet.growUp();
      expect(virtualPet.age).toBe(2);
  })
  it('should increase pet hunger by 5', () => {
    virtualPet.growUp();
    expect(virtualPet.hunger).toBe(5);
  })
  it('should decrease pet fitness by 3', () => {
    virtualPet.growUp();
    expect(virtualPet.fitness).toBe(6);
    virtualPet.growUp();
    expect(virtualPet.fitness).toBe(3);
  })
  it('should throw an exception if deceased', () => {
    virtualPet.age = 31;
    virtualPet.hunger = 11;
    virtualPet.fitness = 0;
    expect(() => virtualPet.growUp()).toThrow('Your pet is sadly no longer alive');
  })
})

describe('walk', () => {
  it('should increase fitness by 4', () => {
      virtualPet.fitness = 3;
      virtualPet.walk();
      expect(virtualPet.fitness).toBe(7);
  })
  it('should have a max fitness of 10', () => {
      virtualPet.fitness = 9;
      virtualPet.walk();
      expect(virtualPet.fitness).toBe(10);
      virtualPet.fitness = 3;
      virtualPet.walk();
      expect(virtualPet.fitness).toBe(7);
  })
  it('should throw an exception with "Your pet is sadly no longer alive"', () => {
      virtualPet.fitness = 0;
      expect(() => virtualPet.walk()).toThrow('Your pet is sadly no longer alive');
  })
})

describe ('feed', () => {
  it('should decrease pet hunger by 3', () => {
    virtualPet.hunger = 5
    virtualPet.feed()
    expect(virtualPet.hunger).toBe(2)
    virtualPet.hunger = 7
    virtualPet.feed()
    virtualPet.feed()
    virtualPet.feed()
    expect(virtualPet.hunger).toBe(0)
  })
  it('should throw an exception with "Your pet is sadly no longer alive"', () => {
    virtualPet.hunger = 15;
    expect(() => virtualPet.feed()).toThrow('Your pet is sadly no longer alive');
})

describe('checkUp', () => {
  it('should return "I need a walk" when fitness is < 3', () => {
    virtualPet.fitness = 2
    expect(virtualPet.checkUp()).toEqual('I need a walk')
  })
  it('should let you know if the pet is hungry and needs a walk', () => {
    virtualPet.fitness = 2
    virtualPet.hunger = 5
    expect(virtualPet.checkUp()).toEqual('I am hungry AND need a walk')
  })
  it('should return "I am hungry" when hunger is > 5', () => {
    virtualPet.hunger = 5
    expect(virtualPet.checkUp()).toEqual('I am hungry')
  })
  it('should return "Your pet is sadly no longer alive" when age > 30 hunger === 10 & fitness === 0', () => {
    virtualPet.age = 31;
    virtualPet.hunger = 10;
    virtualPet.fitness = 0;
    expect(virtualPet.checkUp()).toBe('Your pet is no longer alive');
  })
})

describe ('isAlive', () => {
  it('returns true when age < 30', () => {
    virtualPet.age = 25;
    expect(virtualPet.isAlive).toBe(true);
  })
  it('returns true when fitness > 0', () => {
    expect(virtualPet.isAlive).toBe(true);
  })
  it('returns true when hunger < 10', () => {
      virtualPet.hunger = 9;
      expect(virtualPet.isAlive).toBe(true);
  })
  it('returns false when age > 30 hunger === 10 & fitness === 0', () => {
      virtualPet.age = 30;
      virtualPet.hunger = 10;
      virtualPet.fitness = 0;
      expect(virtualPet.isAlive).toBe(false);
  })
})

describe ('haveBaby', () => {
  it('should have an array for children', () => {
    virtualPet.haveBaby('Harry')
    expect(virtualPet.children).toBeInstanceOf(Array)
  })
})